// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
let cube = 2*2*2

// Strings Using Template Literal
// Uses the backticks (` `)
message = `The cube of 2 is  ${cube}. `
console.log(`Perfect!!! ${message}`)


	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
	console.log(" ");
	const address = ["Type C Road, ", "NBP Reservation, ", "Poblacion, Muntinlupa"]
	const [street, subdivision, city] = address

	console.log(`I live at ${street} ${subdivision} ${city}. Welcome Guys!`)

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
	console.log(" ");
	const animal = {
		breed: "saltwater crocodile",
		petName: "Lolong",
		weight: 1000,
		mesurement:'20ft 3in'
	}

	const {breed, petName, weight, mesurement} = animal
	console.log(`${petName}. was a ${breed}. He weight at ${weight} kgs with a measurement of ${mesurement}.`)
	console.log("")



	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:
	const numbers = [1,2,3,4,15]

	numbers.forEach((number)=>{
		console.log(`${number} Congratualation you made it!!!`)
	})
/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:
	class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog()
console.log(myDog)

myDog.name = "Frankie"
myDog.age = 5
myDog.breed = "Miniature dachshund"

console.log(myDog)
